package com.ruoyi.project.hr.attendance.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 考勤管理对象 hr_attendance
 * 
 * @author d
 * @date 2020-04-17
 */
public class HrAttendance extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** null */
    @Excel(name = "null")
    private Long employeeNumber;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date day;

    /**  */
    @Excel(name = "")
    private Long timeType;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /**  */
    @Excel(name = "")
    private Long startType;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /**  */
    @Excel(name = "")
    private Long endType;

    /**  */
    @Excel(name = "")
    private Long workType;

    /** null */
    @Excel(name = "null")
    private String notes;


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEmployeeNumber(Long employeeNumber) 
    {
        this.employeeNumber = employeeNumber;
    }

    public Long getEmployeeNumber() 
    {
        return employeeNumber;
    }
    public void setDay(Date day) 
    {
        this.day = day;
    }

    public Date getDay() 
    {
        return day;
    }
    public void setTimeType(Long timeType) 
    {
        this.timeType = timeType;
    }

    public Long getTimeType() 
    {
        return timeType;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setStartType(Long startType) 
    {
        this.startType = startType;
    }

    public Long getStartType() 
    {
        return startType;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setEndType(Long endType) 
    {
        this.endType = endType;
    }

    public Long getEndType() 
    {
        return endType;
    }
    public void setWorkType(Long workType) 
    {
        this.workType = workType;
    }

    public Long getWorkType() 
    {
        return workType;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("employeeNumber", getEmployeeNumber())
            .append("day", getDay())
            .append("timeType", getTimeType())
            .append("startTime", getStartTime())
            .append("startType", getStartType())
            .append("endTime", getEndTime())
            .append("endType", getEndType())
            .append("workType", getWorkType())
            .append("notes", getNotes())
            .toString();
    }
}
