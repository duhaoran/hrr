package com.ruoyi.project.hr.attendance.mapper;

import java.util.Date;
import java.util.List;
import com.ruoyi.project.hr.attendance.domain.HrAttendance;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 考勤管理Mapper接口
 * 
 * @author d
 * @date 2020-04-17
 */
@Repository
public interface HrAttendanceMapper 
{
    /**
     * 查询考勤管理
     * 
     * @param id 考勤管理ID
     * @return 考勤管理
     */
    public HrAttendance selectHrAttendanceById(Long id);

    /**
     * 查询考勤管理列表
     * 
     * @param hrAttendance 考勤管理
     * @return 考勤管理集合
     */
    public List<HrAttendance> selectHrAttendanceList(HrAttendance hrAttendance);

    /**
     * 新增考勤管理
     * 
     * @param hrAttendance 考勤管理
     * @return 结果
     */
    public int insertHrAttendance(HrAttendance hrAttendance);

    /**
     * 修改考勤管理
     * 
     * @param hrAttendance 考勤管理
     * @return 结果
     */
    public int updateHrAttendance(HrAttendance hrAttendance);

    /**
     * 删除考勤管理
     * 
     * @param id 考勤管理ID
     * @return 结果
     */
    public int deleteHrAttendanceById(Long id);

    /**
     * 批量删除考勤管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHrAttendanceByIds(String[] ids);

    HrAttendance selectHrAttendanceByNumAndDay(@Param(value = "employeeNumber") Long employeeNumber,
                                               @Param(value = "day") String day);
}
