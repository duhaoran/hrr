package com.ruoyi.project.hr.attendance.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.hr.attendance.mapper.HrAttendanceMapper;
import com.ruoyi.project.hr.attendance.domain.HrAttendance;
import com.ruoyi.project.hr.attendance.service.IHrAttendanceService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 考勤管理Service业务层处理
 * 
 * @author d
 * @date 2020-04-17
 */
@Service
public class HrAttendanceServiceImpl implements IHrAttendanceService 
{
    @Autowired
    private HrAttendanceMapper hrAttendanceMapper;

    /**
     * 查询考勤管理
     * 
     * @param id 考勤管理ID
     * @return 考勤管理
     */
    @Override
    public HrAttendance selectHrAttendanceById(Long id)
    {
        return hrAttendanceMapper.selectHrAttendanceById(id);
    }

    /**
     * 查询考勤管理列表
     * 
     * @param hrAttendance 考勤管理
     * @return 考勤管理
     */
    @Override
    public List<HrAttendance> selectHrAttendanceList(HrAttendance hrAttendance)
    {
        return hrAttendanceMapper.selectHrAttendanceList(hrAttendance);
    }

    /**
     * 新增考勤管理
     * 
     * @param hrAttendance 考勤管理
     * @return 结果
     */
    @Override
    public int insertHrAttendance(HrAttendance hrAttendance)
    {
        return hrAttendanceMapper.insertHrAttendance(hrAttendance);
    }

    /**
     * 修改考勤管理
     * 
     * @param hrAttendance 考勤管理
     * @return 结果
     */
    @Override
    public int updateHrAttendance(HrAttendance hrAttendance)
    {
        return hrAttendanceMapper.updateHrAttendance(hrAttendance);
    }

    /**
     * 删除考勤管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHrAttendanceByIds(String ids)
    {
        return hrAttendanceMapper.deleteHrAttendanceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除考勤管理信息
     * 
     * @param id 考勤管理ID
     * @return 结果
     */
    @Override
    public int deleteHrAttendanceById(Long id)
    {
        return hrAttendanceMapper.deleteHrAttendanceById(id);
    }
}
