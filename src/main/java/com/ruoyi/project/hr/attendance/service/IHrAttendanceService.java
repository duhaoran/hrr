package com.ruoyi.project.hr.attendance.service;

import java.util.List;
import com.ruoyi.project.hr.attendance.domain.HrAttendance;

/**
 * 考勤管理Service接口
 * 
 * @author d
 * @date 2020-04-17
 */
public interface IHrAttendanceService 
{
    /**
     * 查询考勤管理
     * 
     * @param id 考勤管理ID
     * @return 考勤管理
     */
    public HrAttendance selectHrAttendanceById(Long id);

    /**
     * 查询考勤管理列表
     * 
     * @param hrAttendance 考勤管理
     * @return 考勤管理集合
     */
    public List<HrAttendance> selectHrAttendanceList(HrAttendance hrAttendance);

    /**
     * 新增考勤管理
     * 
     * @param hrAttendance 考勤管理
     * @return 结果
     */
    public int insertHrAttendance(HrAttendance hrAttendance);

    /**
     * 修改考勤管理
     * 
     * @param hrAttendance 考勤管理
     * @return 结果
     */
    public int updateHrAttendance(HrAttendance hrAttendance);

    /**
     * 批量删除考勤管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHrAttendanceByIds(String ids);

    /**
     * 删除考勤管理信息
     * 
     * @param id 考勤管理ID
     * @return 结果
     */
    public int deleteHrAttendanceById(Long id);
}
