package com.ruoyi.project.hr.attendance.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.hr.attendance.domain.HrAttendance;
import com.ruoyi.project.hr.attendance.service.IHrAttendanceService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 考勤管理Controller
 * 
 * @author d
 * @date 2020-04-17
 */
@Controller
@RequestMapping("/system/attendance")
public class HrAttendanceController extends BaseController
{
    private String prefix = "system/attendance";

    @Autowired
    private IHrAttendanceService hrAttendanceService;

    @RequiresPermissions("system:attendance:view")
    @GetMapping()
    public String attendance()
    {
        return prefix + "/attendance";
    }

    /**
     * 查询考勤管理列表
     */
    @RequiresPermissions("system:attendance:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HrAttendance hrAttendance)
    {
        startPage();
        List<HrAttendance> list = hrAttendanceService.selectHrAttendanceList(hrAttendance);
        return getDataTable(list);
    }

    /**
     * 导出考勤管理列表
     */
    @RequiresPermissions("system:attendance:export")
    @Log(title = "考勤管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HrAttendance hrAttendance)
    {
        List<HrAttendance> list = hrAttendanceService.selectHrAttendanceList(hrAttendance);
        ExcelUtil<HrAttendance> util = new ExcelUtil<HrAttendance>(HrAttendance.class);
        return util.exportExcel(list, "attendance");
    }

    /**
     * 新增考勤管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存考勤管理
     */
    @RequiresPermissions("system:attendance:add")
    @Log(title = "考勤管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HrAttendance hrAttendance)
    {
        hrAttendance.setStartType(1L);
        hrAttendance.setEndType(1L);
        return toAjax(hrAttendanceService.insertHrAttendance(hrAttendance));
    }

    /**
     * 修改考勤管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HrAttendance hrAttendance = hrAttendanceService.selectHrAttendanceById(id);
        mmap.put("hrAttendance", hrAttendance);
        return prefix + "/edit";
    }

    /**
     * 修改保存考勤管理
     */
    @RequiresPermissions("system:attendance:edit")
    @Log(title = "考勤管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HrAttendance hrAttendance)
    {
        return toAjax(hrAttendanceService.updateHrAttendance(hrAttendance));
    }

    /**
     * 删除考勤管理
     */
    @RequiresPermissions("system:attendance:remove")
    @Log(title = "考勤管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hrAttendanceService.deleteHrAttendanceByIds(ids));
    }
}
