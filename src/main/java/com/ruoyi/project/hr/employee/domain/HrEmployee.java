package com.ruoyi.project.hr.employee.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 员工管理对象 hr_employee
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
public class HrEmployee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** null */
    @Excel(name = "null")
    private Long employeeNumber;

    /** null */
    @Excel(name = "null")
    private String name;

    /**  */
    @Excel(name = "")
    private Long gender;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** null */
    @Excel(name = "null")
    private String telephone;

    /** null */
    @Excel(name = "null")
    private String email;

    /** null */
    @Excel(name = "null")
    private String address;

    /** null */
    @Excel(name = "null")
    private String photo;

    /** null */
    @Excel(name = "null")
    private String education;

    /** null */
    @Excel(name = "null")
    private Long departmentNumber;

    /** null */
    @Excel(name = "null")
    private Long positionNumber;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** null */
    @Excel(name = "null")
    private String password;

    /** null */
    @Excel(name = "null")
    private String notes;


    private Long sing;

    public Long getSing() {
        return sing;
    }

    public void setSing(Long sing) {
        this.sing = sing;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEmployeeNumber(Long employeeNumber) 
    {
        this.employeeNumber = employeeNumber;
    }

    public Long getEmployeeNumber() 
    {
        return employeeNumber;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGender(Long gender) 
    {
        this.gender = gender;
    }

    public Long getGender() 
    {
        return gender;
    }
    public void setBirthday(Date birthday) 
    {
        this.birthday = birthday;
    }

    public Date getBirthday() 
    {
        return birthday;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPhoto(String photo) 
    {
        this.photo = photo;
    }

    public String getPhoto() 
    {
        return photo;
    }
    public void setEducation(String education) 
    {
        this.education = education;
    }

    public String getEducation() 
    {
        return education;
    }
    public void setDepartmentNumber(Long departmentNumber) 
    {
        this.departmentNumber = departmentNumber;
    }

    public Long getDepartmentNumber() 
    {
        return departmentNumber;
    }
    public void setPositionNumber(Long positionNumber) 
    {
        this.positionNumber = positionNumber;
    }

    public Long getPositionNumber() 
    {
        return positionNumber;
    }
    public void setInTime(Date inTime) 
    {
        this.inTime = inTime;
    }

    public Date getInTime() 
    {
        return inTime;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("employeeNumber", getEmployeeNumber())
            .append("name", getName())
            .append("gender", getGender())
            .append("birthday", getBirthday())
            .append("telephone", getTelephone())
            .append("email", getEmail())
            .append("address", getAddress())
            .append("photo", getPhoto())
            .append("education", getEducation())
            .append("departmentNumber", getDepartmentNumber())
            .append("positionNumber", getPositionNumber())
            .append("inTime", getInTime())
            .append("password", getPassword())
            .append("notes", getNotes())
            .toString();
    }
}
