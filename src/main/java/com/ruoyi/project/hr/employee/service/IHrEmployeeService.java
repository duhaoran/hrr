package com.ruoyi.project.hr.employee.service;

import java.util.List;
import com.ruoyi.project.hr.employee.domain.HrEmployee;

/**
 * 员工管理Service接口
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
public interface IHrEmployeeService 
{
    /**
     * 查询员工管理
     * 
     * @param id 员工管理ID
     * @return 员工管理
     */
    public HrEmployee selectHrEmployeeById(Long id);

    /**
     * 查询员工管理列表
     * 
     * @param hrEmployee 员工管理
     * @return 员工管理集合
     */
    public List<HrEmployee> selectHrEmployeeList(HrEmployee hrEmployee);

    /**
     * 新增员工管理
     * 
     * @param hrEmployee 员工管理
     * @return 结果
     */
    public int insertHrEmployee(HrEmployee hrEmployee);

    /**
     * 修改员工管理
     * 
     * @param hrEmployee 员工管理
     * @return 结果
     */
    public int updateHrEmployee(HrEmployee hrEmployee);

    /**
     * 批量删除员工管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHrEmployeeByIds(String ids);

    /**
     * 删除员工管理信息
     * 
     * @param id 员工管理ID
     * @return 结果
     */
    public int deleteHrEmployeeById(Long id);

    int singIn(String ids);

    int singOut(String ids);

    int leave(String toString);
}
