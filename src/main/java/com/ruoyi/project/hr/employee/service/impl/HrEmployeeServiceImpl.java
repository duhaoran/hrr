package com.ruoyi.project.hr.employee.service.impl;

import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.hr.attendance.domain.HrAttendance;
import com.ruoyi.project.hr.attendance.mapper.HrAttendanceMapper;
import com.ruoyi.project.hr.employee.domain.HrEmployee;
import com.ruoyi.project.hr.employee.mapper.HrEmployeeMapper;
import com.ruoyi.project.hr.employee.service.IHrEmployeeService;
import com.ruoyi.project.hr.history.domain.HrHistory;
import com.ruoyi.project.hr.history.mapper.HrHistoryMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * 员工管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Service
public class HrEmployeeServiceImpl implements IHrEmployeeService 
{
    @Autowired
    private HrEmployeeMapper hrEmployeeMapper;
    @Autowired
    private HrAttendanceMapper hrAttendanceMapper;
    @Autowired
    private HrHistoryMapper hrHistoryMapper;

    private static final String MIDDLE_TIME = "12:00:00";

    /**
     * 查询员工管理
     * 
     * @param id 员工管理ID
     * @return 员工管理
     */
    @Override
    public HrEmployee selectHrEmployeeById(Long id)
    {
        return hrEmployeeMapper.selectHrEmployeeById(id);
    }

    /**
     * 查询员工管理列表
     * 
     * @param hrEmployee 员工管理
     * @return 员工管理
     */
    @Override
    public List<HrEmployee> selectHrEmployeeList(HrEmployee hrEmployee)
    {
        hrEmployee.setSing(0L);
        return hrEmployeeMapper.selectHrEmployeeList(hrEmployee);
    }

    /**
     * 新增员工管理
     * 
     * @param hrEmployee 员工管理
     * @return 结果
     */
    @Override
    public int insertHrEmployee(HrEmployee hrEmployee)
    {
        return hrEmployeeMapper.insertHrEmployee(hrEmployee);
    }

    /**
     * 修改员工管理
     * 
     * @param hrEmployee 员工管理
     * @return 结果
     */
    @Override
    public int updateHrEmployee(HrEmployee hrEmployee)
    {
        return hrEmployeeMapper.updateHrEmployee(hrEmployee);
    }

    /**
     * 删除员工管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHrEmployeeByIds(String ids)
    {
        return hrEmployeeMapper.deleteHrEmployeeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除员工管理信息
     * 
     * @param id 员工管理ID
     * @return 结果
     */
    @Override
    public int deleteHrEmployeeById(Long id)
    {
        return hrEmployeeMapper.deleteHrEmployeeById(id);
    }

    @Override
    public int singIn(String ids) {
        HrEmployee hrEmployee = hrEmployeeMapper.selectHrEmployeeById(Long.parseLong(ids));
        HrAttendance hrAttendance = new HrAttendance();
        hrAttendance.setDay(new Date());
        hrAttendance.setEmployeeNumber(hrEmployee.getEmployeeNumber());
        hrAttendance.setStartTime(new Date());
        hrAttendance.setStartType(1L);
        hrAttendance.setTimeType(1L);
        hrAttendanceMapper.insertHrAttendance(hrAttendance);
        return 1;
    }

    @Override
    public int singOut(String ids) {
        HrEmployee hrEmployee = hrEmployeeMapper.selectHrEmployeeById(Long.parseLong(ids));
        HrAttendance hrAttendance =
                hrAttendanceMapper.selectHrAttendanceByNumAndDay(
                    Long.valueOf(hrEmployee.getEmployeeNumber()),
                    DateFormat.getDateInstance().format(new Date()));
        if (hrAttendance == null) {
            System.out.println("签退失败，今日未进行签到");
            return 0;
        }

//        String s=DateFormat.getDateInstance().format(new Date()) + " " +MIDDLE_TIME;

        hrAttendance.setEndTime(new Date());
        hrAttendance.setEndType(1L);
        hrAttendanceMapper.updateHrAttendance(hrAttendance);

        return 1;
    }

    @Override
    @Transactional
    public int leave(String ids) {
        HrEmployee hrEmployee = hrEmployeeMapper.selectHrEmployeeById(Long.parseLong(ids));
        hrEmployee.setSing(1L);
        hrEmployeeMapper.updateHrEmployee(hrEmployee);

        HrHistory hrHistory = new HrHistory();
        BeanUtils.copyProperties(hrEmployee, hrHistory);
        hrHistory.setStatus(2L); // 离职
        hrHistory.setOutTime(new Date());
        hrHistoryMapper.insertHrHistory(hrHistory);

        return 1;
    }

}
