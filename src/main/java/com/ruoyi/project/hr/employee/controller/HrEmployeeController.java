package com.ruoyi.project.hr.employee.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.hr.employee.domain.HrEmployee;
import com.ruoyi.project.hr.employee.service.IHrEmployeeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 员工管理Controller
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Controller
@RequestMapping("/system/employee")
public class HrEmployeeController extends BaseController
{
    private String prefix = "system/employee";

    @Autowired
    private IHrEmployeeService hrEmployeeService;

    @RequiresPermissions("system:employee:view")
    @GetMapping()
    public String employee()
    {
        return prefix + "/employee";
    }

    /**
     * 查询员工管理列表
     */
    @RequiresPermissions("system:employee:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HrEmployee hrEmployee)
    {
        startPage();
        List<HrEmployee> list = hrEmployeeService.selectHrEmployeeList(hrEmployee);
        return getDataTable(list);
    }

    /**
     * 导出员工管理列表
     */
    @RequiresPermissions("system:employee:export")
    @Log(title = "员工管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HrEmployee hrEmployee)
    {
        List<HrEmployee> list = hrEmployeeService.selectHrEmployeeList(hrEmployee);
        ExcelUtil<HrEmployee> util = new ExcelUtil<HrEmployee>(HrEmployee.class);
        return util.exportExcel(list, "employee");
    }

    /**
     * 新增员工管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存员工管理
     */
    @RequiresPermissions("system:employee:add")
    @Log(title = "员工管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HrEmployee hrEmployee)
    {
        return toAjax(hrEmployeeService.insertHrEmployee(hrEmployee));
    }

    /**
     * 修改员工管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HrEmployee hrEmployee = hrEmployeeService.selectHrEmployeeById(id);
        mmap.put("hrEmployee", hrEmployee);
        return prefix + "/edit";
    }

    /**
     * 修改保存员工管理
     */
    @RequiresPermissions("system:employee:edit")
    @Log(title = "员工管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HrEmployee hrEmployee)
    {
        return toAjax(hrEmployeeService.updateHrEmployee(hrEmployee));
    }

    /**
     * 删除员工管理
     */
    @RequiresPermissions("system:employee:remove")
    @Log(title = "员工管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hrEmployeeService.deleteHrEmployeeByIds(ids));
    }

    // 签到
    @RequiresPermissions("system:employee:signIn")
    @GetMapping( "/signIn/{id}")
    @ResponseBody
    public AjaxResult singIn(@PathVariable("id") Long id)
    {
        return toAjax(hrEmployeeService.singIn(id.toString()));
    }

    // 签退
    @RequiresPermissions("system:employee:signOut")
    @GetMapping( "/signOut/{id}")
    @ResponseBody
    public AjaxResult singOut(@PathVariable("id") Long id)
    {
        int i = hrEmployeeService.singOut(id.toString());
        if (i == 0) {
            return toAjax(0);
        }
        return toAjax(1);
    }

    // 离职
     @RequiresPermissions("system:employee:leave")
    @GetMapping( "/leave/{id}")
    @ResponseBody
    public AjaxResult leave(@PathVariable("id") Long id)
    {
        int i = hrEmployeeService.leave(id.toString());
        if (i == 0) {
            return toAjax(0);
        }
        return toAjax(1);
    }
}
