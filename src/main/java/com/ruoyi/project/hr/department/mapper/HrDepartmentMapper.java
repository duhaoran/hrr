package com.ruoyi.project.hr.department.mapper;

import java.util.List;
import com.ruoyi.project.hr.department.domain.HrDepartment;
import org.springframework.stereotype.Repository;

/**
 * 部门管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Repository
public interface HrDepartmentMapper 
{
    /**
     * 查询部门管理
     * 
     * @param id 部门管理ID
     * @return 部门管理
     */
    public HrDepartment selectHrDepartmentById(Long id);

    /**
     * 查询部门管理列表
     * 
     * @param hrDepartment 部门管理
     * @return 部门管理集合
     */
    public List<HrDepartment> selectHrDepartmentList(HrDepartment hrDepartment);

    /**
     * 新增部门管理
     * 
     * @param hrDepartment 部门管理
     * @return 结果
     */
    public int insertHrDepartment(HrDepartment hrDepartment);

    /**
     * 修改部门管理
     * 
     * @param hrDepartment 部门管理
     * @return 结果
     */
    public int updateHrDepartment(HrDepartment hrDepartment);

    /**
     * 删除部门管理
     * 
     * @param id 部门管理ID
     * @return 结果
     */
    public int deleteHrDepartmentById(Long id);

    /**
     * 批量删除部门管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHrDepartmentByIds(String[] ids);
}
