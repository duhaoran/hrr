package com.ruoyi.project.hr.department.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.hr.department.domain.HrDepartment;
import com.ruoyi.project.hr.department.service.IHrDepartmentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 部门管理Controller
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Controller
@RequestMapping("/system/department")
public class HrDepartmentController extends BaseController
{
    private String prefix = "system/department";

    @Autowired
    private IHrDepartmentService hrDepartmentService;

    @RequiresPermissions("system:department:view")
    @GetMapping()
    public String department()
    {
        return prefix + "/department";
    }

    /**
     * 查询部门管理列表
     */
    @RequiresPermissions("system:department:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HrDepartment hrDepartment)
    {
        startPage();
        List<HrDepartment> list = hrDepartmentService.selectHrDepartmentList(hrDepartment);
        return getDataTable(list);
    }

    /**
     * 导出部门管理列表
     */
    @RequiresPermissions("system:department:export")
    @Log(title = "部门管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HrDepartment hrDepartment)
    {
        List<HrDepartment> list = hrDepartmentService.selectHrDepartmentList(hrDepartment);
        ExcelUtil<HrDepartment> util = new ExcelUtil<HrDepartment>(HrDepartment.class);
        return util.exportExcel(list, "department");
    }

    /**
     * 新增部门管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存部门管理
     */
    @RequiresPermissions("system:department:add")
    @Log(title = "部门管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HrDepartment hrDepartment)
    {
        return toAjax(hrDepartmentService.insertHrDepartment(hrDepartment));
    }

    /**
     * 修改部门管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HrDepartment hrDepartment = hrDepartmentService.selectHrDepartmentById(id);
        mmap.put("hrDepartment", hrDepartment);
        return prefix + "/edit";
    }

    /**
     * 修改保存部门管理
     */
    @RequiresPermissions("system:department:edit")
    @Log(title = "部门管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HrDepartment hrDepartment)
    {
        return toAjax(hrDepartmentService.updateHrDepartment(hrDepartment));
    }

    /**
     * 删除部门管理
     */
    @RequiresPermissions("system:department:remove")
    @Log(title = "部门管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hrDepartmentService.deleteHrDepartmentByIds(ids));
    }
}
