package com.ruoyi.project.hr.department.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.hr.department.mapper.HrDepartmentMapper;
import com.ruoyi.project.hr.department.domain.HrDepartment;
import com.ruoyi.project.hr.department.service.IHrDepartmentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 部门管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Service
public class HrDepartmentServiceImpl implements IHrDepartmentService 
{
    @Autowired
    private HrDepartmentMapper hrDepartmentMapper;

    /**
     * 查询部门管理
     * 
     * @param id 部门管理ID
     * @return 部门管理
     */
    @Override
    public HrDepartment selectHrDepartmentById(Long id)
    {
        return hrDepartmentMapper.selectHrDepartmentById(id);
    }

    /**
     * 查询部门管理列表
     * 
     * @param hrDepartment 部门管理
     * @return 部门管理
     */
    @Override
    public List<HrDepartment> selectHrDepartmentList(HrDepartment hrDepartment)
    {
        return hrDepartmentMapper.selectHrDepartmentList(hrDepartment);
    }

    /**
     * 新增部门管理
     * 
     * @param hrDepartment 部门管理
     * @return 结果
     */
    @Override
    public int insertHrDepartment(HrDepartment hrDepartment)
    {
        return hrDepartmentMapper.insertHrDepartment(hrDepartment);
    }

    /**
     * 修改部门管理
     * 
     * @param hrDepartment 部门管理
     * @return 结果
     */
    @Override
    public int updateHrDepartment(HrDepartment hrDepartment)
    {
        return hrDepartmentMapper.updateHrDepartment(hrDepartment);
    }

    /**
     * 删除部门管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHrDepartmentByIds(String ids)
    {
        return hrDepartmentMapper.deleteHrDepartmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除部门管理信息
     * 
     * @param id 部门管理ID
     * @return 结果
     */
    @Override
    public int deleteHrDepartmentById(Long id)
    {
        return hrDepartmentMapper.deleteHrDepartmentById(id);
    }
}
