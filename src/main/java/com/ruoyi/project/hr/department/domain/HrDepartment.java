package com.ruoyi.project.hr.department.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 部门管理对象 hr_department
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
public class HrDepartment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** null */
    @Excel(name = "null")
    private Long departmentNumber;

    /** null */
    @Excel(name = "null")
    private String name;

    /** null */
    @Excel(name = "null")
    private String manager;

    /** null */
    @Excel(name = "null")
    private String telephone;

    /** null */
    @Excel(name = "null")
    private String address;

    /** null */
    @Excel(name = "null")
    private String notes;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDepartmentNumber(Long departmentNumber) 
    {
        this.departmentNumber = departmentNumber;
    }

    public Long getDepartmentNumber() 
    {
        return departmentNumber;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setManager(String manager) 
    {
        this.manager = manager;
    }

    public String getManager() 
    {
        return manager;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("departmentNumber", getDepartmentNumber())
            .append("name", getName())
            .append("manager", getManager())
            .append("telephone", getTelephone())
            .append("address", getAddress())
            .append("notes", getNotes())
            .toString();
    }
}
