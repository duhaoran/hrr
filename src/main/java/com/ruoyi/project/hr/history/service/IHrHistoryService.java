package com.ruoyi.project.hr.history.service;

import java.util.List;
import com.ruoyi.project.hr.history.domain.HrHistory;

/**
 * 历史记录Service接口
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
public interface IHrHistoryService 
{
    /**
     * 查询历史记录
     * 
     * @param id 历史记录ID
     * @return 历史记录
     */
    public HrHistory selectHrHistoryById(Long id);

    /**
     * 查询历史记录列表
     * 
     * @param hrHistory 历史记录
     * @return 历史记录集合
     */
    public List<HrHistory> selectHrHistoryList(HrHistory hrHistory);

    /**
     * 新增历史记录
     * 
     * @param hrHistory 历史记录
     * @return 结果
     */
    public int insertHrHistory(HrHistory hrHistory);

    /**
     * 修改历史记录
     * 
     * @param hrHistory 历史记录
     * @return 结果
     */
    public int updateHrHistory(HrHistory hrHistory);

    /**
     * 批量删除历史记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHrHistoryByIds(String ids);

    /**
     * 删除历史记录信息
     * 
     * @param id 历史记录ID
     * @return 结果
     */
    public int deleteHrHistoryById(Long id);
}
