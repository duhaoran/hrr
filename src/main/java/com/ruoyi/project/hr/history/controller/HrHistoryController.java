package com.ruoyi.project.hr.history.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.hr.history.domain.HrHistory;
import com.ruoyi.project.hr.history.service.IHrHistoryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 历史记录Controller
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Controller
@RequestMapping("/system/history")
public class HrHistoryController extends BaseController
{
    private String prefix = "system/history";

    @Autowired
    private IHrHistoryService hrHistoryService;

    @RequiresPermissions("system:history:view")
    @GetMapping()
    public String history()
    {
        return prefix + "/history";
    }

    /**
     * 查询历史记录列表
     */
    @RequiresPermissions("system:history:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HrHistory hrHistory)
    {
        startPage();
        List<HrHistory> list = hrHistoryService.selectHrHistoryList(hrHistory);
        return getDataTable(list);
    }

    /**
     * 导出历史记录列表
     */
    @RequiresPermissions("system:history:export")
    @Log(title = "历史记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HrHistory hrHistory)
    {
        List<HrHistory> list = hrHistoryService.selectHrHistoryList(hrHistory);
        ExcelUtil<HrHistory> util = new ExcelUtil<HrHistory>(HrHistory.class);
        return util.exportExcel(list, "history");
    }

    /**
     * 新增历史记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存历史记录
     */
    @RequiresPermissions("system:history:add")
    @Log(title = "历史记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HrHistory hrHistory)
    {
        return toAjax(hrHistoryService.insertHrHistory(hrHistory));
    }

    /**
     * 修改历史记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        HrHistory hrHistory = hrHistoryService.selectHrHistoryById(id);
        mmap.put("hrHistory", hrHistory);
        return prefix + "/edit";
    }

    /**
     * 修改保存历史记录
     */
    @RequiresPermissions("system:history:edit")
    @Log(title = "历史记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HrHistory hrHistory)
    {
        return toAjax(hrHistoryService.updateHrHistory(hrHistory));
    }

    /**
     * 删除历史记录
     */
    @RequiresPermissions("system:history:remove")
    @Log(title = "历史记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hrHistoryService.deleteHrHistoryByIds(ids));
    }
}
