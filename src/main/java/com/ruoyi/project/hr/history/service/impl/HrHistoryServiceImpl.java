package com.ruoyi.project.hr.history.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.hr.history.mapper.HrHistoryMapper;
import com.ruoyi.project.hr.history.domain.HrHistory;
import com.ruoyi.project.hr.history.service.IHrHistoryService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 历史记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-17
 */
@Service
public class HrHistoryServiceImpl implements IHrHistoryService 
{
    @Autowired
    private HrHistoryMapper hrHistoryMapper;

    /**
     * 查询历史记录
     * 
     * @param id 历史记录ID
     * @return 历史记录
     */
    @Override
    public HrHistory selectHrHistoryById(Long id)
    {
        return hrHistoryMapper.selectHrHistoryById(id);
    }

    /**
     * 查询历史记录列表
     * 
     * @param hrHistory 历史记录
     * @return 历史记录
     */
    @Override
    public List<HrHistory> selectHrHistoryList(HrHistory hrHistory)
    {
        return hrHistoryMapper.selectHrHistoryList(hrHistory);
    }

    /**
     * 新增历史记录
     * 
     * @param hrHistory 历史记录
     * @return 结果
     */
    @Override
    public int insertHrHistory(HrHistory hrHistory)
    {
        return hrHistoryMapper.insertHrHistory(hrHistory);
    }

    /**
     * 修改历史记录
     * 
     * @param hrHistory 历史记录
     * @return 结果
     */
    @Override
    public int updateHrHistory(HrHistory hrHistory)
    {
        return hrHistoryMapper.updateHrHistory(hrHistory);
    }

    /**
     * 删除历史记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHrHistoryByIds(String ids)
    {
        return hrHistoryMapper.deleteHrHistoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除历史记录信息
     * 
     * @param id 历史记录ID
     * @return 结果
     */
    @Override
    public int deleteHrHistoryById(Long id)
    {
        return hrHistoryMapper.deleteHrHistoryById(id);
    }
}
